
import { HOST_BASIC } from '../../../constants/config.js';

/**
 *  新闻公告  相关接口
 */
// 已读操作
export const NEWS_CLICK_READ = `${HOST_BASIC}/je/readFuncEdit`;
// 阅读数
export const NEWS_CLICK_DOUPDATE = `${HOST_BASIC}/je/doUpdate`;
// 导航栏数据
export const NEWS_NAV_DATA = `${HOST_BASIC}/je/dd/dd/getDicItemByCode`;
// load数据
export const POST_SILIANHUAN_DATAS = `${HOST_BASIC}/je/load`;

export const POST_GET_INFO_BY_ID = `${HOST_BASIC}/je/getInfoById`;
