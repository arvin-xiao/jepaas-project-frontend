/*
 * @Descripttion: 登录页面的实例
 * @Author: 张明尧
 * @Date: 2021-01-22 10:58:21
 * @LastEditTime: 2021-02-02 16:41:14
 */
import { removeToken, setToken } from '../util/auth'
import { loginSuccess } from '../util/loginSuccess';
import { decipherStr } from '../util/oauthLogin';
const IS_BIND = 1; // 授权页面传递过来的参数，表明需要授权
import BasicsLogin from './loginType/basics'; // 登录行为的基础类 -- 账号密码登录
import MsgLogin from './loginType/msgLogin'; // 登录行为的扩展类 -- 验证码登录
export default class AppLogin {
  constructor({ vm }) {
    this.vm = vm; // VUE实例的对象
    this.logoPath = ''; // 图片的地址
    this.loginType = new BasicsLogin({vm}); // 当前的登录类型为 --- 账号密码登录
    this.isBind = vm.$route.query.isBind === IS_BIND; // 判断是否为授权绑定流程过来的用户
    this.userCode = vm.$route.query.code; // 用户唯一标识
    this.showPwd = true; // 是否显示当前密码
    this.showModalFlag = false; // 是否显示公司列表
    this.remberStatus = true; // 默认是记住密码
    this.loginInfo = {
      phone: '', // 登录输入的手机号/账户
      pwd: '', // 登录密码
      validateCode: '', // 短信验证码登录的v-model绑定数据
    }; // 当前登录的用户所有信息
    this.loginState = true; // 当前的登录状态(true: 账号密码登录, false: 手机号验证码登录)
    this.companyList = []; // 当前的公司列表
    this.init();
  }
  /**
   * 登录页面初始化后需要执行的方法
   */
  init() {
    if (JE.getLSItem('phone') && JE.getLSItem('password')) {
      this.vm.$set(this.loginInfo, 'phone', decipherStr(JE.getLSItem('phone')));
      this.vm.$set(this.loginInfo, 'pwd', decipherStr(JE.getLSItem('password')));
    }
    this.getApkImg(); // 初始化的时候获取LOGO
    this.valiBindParams(); // 验证是否携带全部需要绑定参数
    this.getLoginPassworld(); // 每次初始化都要获取本地的缓存账号密码添加到用户信息中去
  }

  /**
   * 获取本地localstorage中缓存的登录信息
   */
  getLoginPassworld() {
    if (JE.getLSItem('phone') && JE.getLSItem('password')) {
      this.vm.$set(this.loginInfo, 'phone', decipherStr(JE.getLSItem('phone')));
      this.vm.$set(this.loginInfo, 'pwd', decipherStr(JE.getLSItem('password')));
    }
  }

  /**
   * 获取App图片LOGO
   */
  getApkImg() {
    const config = JE.getApkConfig('APP_LOGIN_ICO');
    const imgKey = config && config.split('*')[1];
    const imgName = config && config.split('*')[0];
    const reg = /\.(png|jpg|gif|jpeg|webp)$/;
    if(imgKey && reg.test(imgName)){
      try {
        // 获取到本地绝对路径【www】然后去掉‘wwww’文件路径，拿到doc绝对路径
        // 截取‘doc/’文件下的路径地址
        // JE.getLSItem(`loginPath_${JE.getApkCode() 可能存在如下值===》2.4.9后迭代为1，2中【suanbanyun2】iOS增量包升级后会替换随机取名
        // -----1、 _doc/2019-0102-1347-8233/download/logImg/logo1.png
        // -----2、 file:///var/mobile/Containers/Data/Application/8CE436AA-6016-47F9-AF41-1594A8BF8DAF/Documents/Pandora/apps/suanbanyun2/doc/2019-0102-1347-8233/download/logImg/logo1.png
        // 拼接图片地址，doc和www在同文件价下
        this.logoPath = `${localStorage.getItem('www').replace(/www\//, '')}${JE.getLSItem(`loginPath_${JE.getApkCode()}`).match(new RegExp('doc/(.*?)$'))[0]}` || JE.buildURL(JE.getFileUrl(imgKey));
      } catch (e) {
        this.logoPath = JE.buildURL(JE.getFileUrl(imgKey));
      }
    }
  }
  /**
   * 验证是否携带全部需要绑定参数
   * 如果是授权流程但是没有用户code，则跳转至授权页面重新获取
   */
  valiBindParams() {
    if (this.isBind && !this.userCode) {
      this.vm.$router.push('/auth');
      return false;
    }
  }

  /**
   * 切换成动态验证码登录
   */
  stateLogin() {
    this.loginState = false;
    this.loginInfo.validateCode = ''; // 置空验证码输入内容
    this.showPwd = true; // 当前pasword类型改为text，用来显示短信验证码
    this.loginType = new MsgLogin({vm: this.vm}); // 切换成验证码登录的拓展类
  }

  /**
   * 个人密码登录
   */
  passworldLogin() {
    this.loginState = true;
    this.loginInfo.pwd = ''; // 当前的密码置空
    this.loginType = new BasicsLogin({vm: this.vm}); // 切换成验证码登录的拓展类
  }

  /**
   * 请求登录接口数据处理
   */
  sendLogin() {
    const param = this.loginInfo;
    this.loginType.setParam(this.loginInfo); // 先设置接口所需要的param值
    // 点击登录调用置顶的类型登录方式
    this.loginType.clickBtn().then(res => {
      if (res && res.success) {
        // 判断是否要记住账号密码,从而更新最新的账号密码
        this.loginType.afterClick({param, remberStatus: this.remberStatus});
        if (res.code === 'OK') {
          this.appLogin_success(res);
        } else if (res.code === 'CHECK') {
          this.companyListCheck(res); // 选择公司
        }
      } else {
        this.showModalFlag = false; // 关闭显示的公司列表
        this.btnReset(); // 输入框是去焦点
        removeToken(); // 删除tonken
        res && res.message && mui.toast(res.message);
      }
    })
  }

  /**
   * 输入框是去焦点
   */
  btnReset() {
    this.vm.$refs.inputBlurName.blur();
    this.vm.$refs.inputBlurPassword.blur();
    mui('.mui-btn')
      .button('reset');
  }

  /**
   * 登录成功后,"不存在"筛选公司。
   * @param {Object} res 登录接口的返回参数
   */
  appLogin_success(res) {
    setToken(res.obj);
      const loginPhone = this.loginInfo.phone || ';';
      if (loginPhone) {
        let set;
        if (JE.getLSItem('loginHistory')) {
          set = new Set(JSON.parse(JE.getLSItem('loginHistory')));
        } else {
          set = new Set();
        }
        set.add(loginPhone);
        JE.setLSItem('loginHistory', JSON.stringify(Array.from(set)));
      }
      // this.hideModal();
      // 登陆成功后传值给首页 请求接口渲染数据，渲染完成后回传登陆页关闭登陆页
      this.showModalFlag = false;
      this.vm.$refs.inputBlurName.blur();
      this.vm.$refs.inputBlurPassword.blur();
      // 登录成功，进行跳转
      loginSuccess(this.vm);
      mui('.mui-btn')
        .button('reset');
  }

  /**
   * 接口返回Check的情况下，需要筛选公司
   * @param {Object} res 登录接口的返回参数
   */
  companyListCheck(res) {
    this.btnReset();
    this.showModalFlag = true;
    this.companyList = JSON.parse(res.obj);
  }

  /**
   * 异常的报错信息
   * @param {String} code 当前的错误编码
   * @param {string} count 错误超过的次数
   */
  errorMsg(code, count) {
    const loginErrMsg = {
      1001: {
        cn1: '账号或者密码错误超过[',
        cn2: ']次,请使用其他方式登录!',
        en1: 'Account or password error more than [',
        en2: '] times, please use other way to log in!',
      },
      1002: {
        cn1: '账号或密码错误,您还剩余[',
        cn2: ']次个人密码登录机会!',
        en1: 'Account number or password error, you still have left [',
        en2: '] personal password login opportunity!',
      },
      1003: {
        cn: '验证码错误!',
        en: 'Verification code error!',
      },
      1004: {
        cn: '用户未认证!',
        en: 'User not authenticated!',
      },
      1005: {
        cn: '认证失败!',
        en: 'Authentication failed!',
      },
      1006: {
        cn: '帐号或密码错误!',
        en: 'Wrong account or password!',
      },
      1007: {
        cn: '状态机不对，请重新登录!',
        en: 'The state machine is not correct, please login again!',
      },
    };
    if ('1001,1002'.indexOf(code) > -1) {
      const cn = loginErrMsg[code].cn1 + count + loginErrMsg[code].cn2;
      mui.toast(cn);
    } else {
      mui.toast(loginErrMsg[code].cn);
    }
  }
}
