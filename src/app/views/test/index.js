/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-14 17:50:57
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-14 17:52:29
 */
import Vue from 'vue';
import App from './index.vue';


Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
  render: h => h(App),
}).$mount('#app');
