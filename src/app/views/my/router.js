/**
 * @Author : ZiQin Zhai
 * @Date : 2020/10/9 13:40
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/10/9 13:40
 * @Description
 * */
import Index from './index.vue';
import rootPage from './rootPage';
import modifyPsd from './pages/modifyPsd/index.vue';

export default [
  {
    path: '/JE-PLUGIN-MY',
    name: 'JE-PLUGIN-MY',
    component: rootPage,
    children: [
      {
        path: '/',
        component: Index,
      },
      {
        path: 'modifyPsd',
        component: modifyPsd,
      },
    ],
  }
];
